# Test project
> Vue.js (front) with Sails.js (backend) project

## Getting started
For full setup project:
```bash
$ make
```

### Prerequisites

#### Start project
Full install-build of project.  
Stop and start docker containers, start API and APP.  
But, without update of containers images.

```bash
$ make
```

#### Stop project

```bash
$ make down
```

#### Rebuild docker images and start project
Stop and start docker containers with update of images.

```bash
$ make rebuild
```
